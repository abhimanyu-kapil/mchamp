<?php 

session_start();

include("lib/db_config.php");
include("lib/DB.php");
include("lib/config.php");
require('libs/Smarty.class.php');
include("function/dbTransations.php");
include("function/custom_functions.php");


$db= new DB();
$con =$db->makeConnection();


if($_REQUEST['event']!='login' && $_REQUEST['event']!='forgotpassword')
	{
		
		$admin=checkUserLogin();
		$registered_urls=array('logout','uploadDailyWinner','uploadDailySingleWinner','upDailyWinner','upWeeklyWinner','getUserName','activateSingleWinner','forgotpassword','uploadWeeklyWinner','uploadWeeklySingleWinner','activateWeeklyWinner');
		
		if($admin===false)
		{
			$_REQUEST['event']="";		
		}
		elseif($_SESSION['admin_type']==0 && !in_array($_REQUEST['event'],$registered_urls))//for sub admin
		{			
			header('location:index.php?event=upDailyWinner');//'upDailyWinner';			
		}
	}

if(isset($_REQUEST['event']) && $_REQUEST['event']!=''){ $event=$_REQUEST['event']; }else{$event=''; }


switch($event)
{
	case 'login':	
		login();	
		//default_page();		
		break;
		
	case 'logout':
		logout();
		break;
		
	 case 'forgotpassword':
	       forgotpassword();
	 break;
		
	 case'Dashboard':
	      Dashboard();
	 break;
	 
	 case'category':
	      MainCategory();
	 break;
	 
	 case'addnewcategory':
	      addnewcategory();
	 break;
	 
	 case'questionset':
	      questionset();
	 break;
	 
	 case'questioncategory':
	      questioncategory();
	 break;
	 
	 case'addnewquestioncategory':
	      addnewquestioncategory();
	 break;
	 
	 case'addnewquestionset':
	     addnewquestionset();
	 break;
	 
	 case'quiz_question':
	      quiz_question();
	 break;
	 
	 case'addnewquestion':
	      add_newquestion();
	 break;
	 
	 case'users':
	      users();
	 break;
	 
	 case'addnewuser':
	      addnewuser();
	 break;
	 
	 case'contest':
	      contest();
	 break;
	 
	case'addnewcontest':
	addnewcontest();
    break;
    
	case'uploadcontest_csv':
	uploadcontest_csv();
    break;
	
	case'uploadquestion_csv':
	uploadquestion_csv();
    break;
	
	case'uploadquestionset_csv':
	uploadquestionset_csv();
    break;
	
	case'feedback':
	Userfeedback();
    break;
	
    case'viewContSch':						//Tushar
    viewContSch();
    break;
    
    case'getData':							//Tushar
    	getData();
    	break;
    
    case'getDataRange':						//Tushar
    	getDataRange();
    	break;
    		
    case'schContQue':						//Tushar
    	schContQue();
    	break;
    	
    case 'viewContSchRange':				//Tushar
    	viewContSchRange();
    	break;

    case 'getContestData':					//Tushar
    	getContestData();
    	break; 
	case 'getContestDataSearch':					//Abhimanyu
    	getContestDataSearch();
    	break;
    case 'movetotemp': 						//Tushar
    	moveToTemp(); 
    	break;
    	
    case 'removeContent':					//Tushar
    	removeContent();
    	break;
    	
    case 'getalltempcontent':				//Tushar
    	getalltempcontent();
    	break;
    case 'upDailyWinner':					//Tushar
    	upDailyWinner();
    	break;
    case 'uploadDailyWinner':				//Tushar
    	uploadDailyWinner();
    	break;
    case 'uploadWeeklyWinner':				//Tushar
    	uploadWeeklyWinner();
    	break;
    case 'activateSingleWinner':			//Tushar
    	activateSingleWinner();
    	break;
    case 'activateWeeklyWinner':			//Tushar
    	activateWeeklyWinner();
    	break;
    case 'upWeeklyWinner';					//Tushar
    	upWeeklyWinner();
    	break;
    case 'getUserName':						//Tushar
    	getUserName();
    	break;
    case 'uploadDailySingleWinner':			//Tushar
    	uploadDailySingleWinner();
    	break;
    case 'uploadWeeklySingleWinner':		//Tushar
    	uploadWeeklySingleWinner();
    	break;
    case 'upQuestionWithoutRep':			//Tushar
    	upQuestionWithoutRep();
    	break;
    case 'onUpQuestionWithoutRep':			//Tushar
    	onUpQuestionWithoutRep();
    	break;
    case 'upQuestion':						//Tushar
    	upQuestion();
    	break;
    case 'onUpQuestion':					//Tushar
    	onUpQuestion();
    	break;
    case 'upQuestionWithoutRepOneQuiz':		//Tushar
    	upQuestionWithoutRepOneQuiz();
    	break;
    case 'onUpQuestionWithoutRepOneQuiz':	//Tushar
    	onUpQuestionWithoutRepOneQuiz();
    	break;
    case 'upQuestionWithoutRepImgQuiz':		//Tushar
    	upQuestionWithoutRepImgQuiz();
    	break;
    case 'onUpQuestionWithoutRepImgQuiz':	//Tushar
    	onUpQuestionWithoutRepImgQuiz();
    	break;
    case 'misReport':						//Tushar
    	//misReport();
		misReport_new();
    	break;
	case 'misReportNew':						//Tushar
    	misReport_new();
    	break; 
	case 'misReportHourly':						//Abhimanyu
    	misReportHourly();
    	break;
    case 'onExportData' :					//Tushar
    	exportData();
    	break;
    case 'emptyTemp':						//Tushar
    	emptyTemp();
    	break;
    case 'onCheckPublishTemp':				//Tushar
    	onCheckPublishTemp();
    	break;
    case 'onPublishTemp' :					//Tushar
    	onPublishTemp();
    	break;
	case'support':
		Usersupport();
   		break;
	
	case'addnewcontest_questionset':
	addnewcontest_questionset();
    break;
	
	case'contestpass':
	ContestPass();
    break;
	
     default:
	//echo $_SERVER['REMOTE_ADDR'];
		default_page();
		break;
}

?>